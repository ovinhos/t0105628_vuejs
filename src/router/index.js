import Vue from 'vue'
import Router from 'vue-router'
import MoviesIndex from '@/components/MoviesIndex'
import MoviesCreate from '@/components/MoviesCreate'
import MoviesYears from '@/components/MoviesYears'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'movies.index',
      component: MoviesIndex
    },
    {
      path: '/movies/create',
      name: 'movies.create',
      component: MoviesCreate
    },
    {
      path: '/movies/year',
      name: 'movies.years',
      component: MoviesYears
    }
  ]
})
